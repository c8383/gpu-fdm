# Artificial Compressibility Finite Difference Method solver
This project is a CUDA implementation of an Artificial Compressibility Method (ACM) Finite Difference Method (FDM) solver. The solver is coupled to a live renderer using GLEW, GLM and OpenGL to enable live feedback of the entire simulation domain.

## Prerequisites
The prerequsities for running this project is a CUDA-capable GPU with the nvidia-cuda-toolkit installed. In addition the project also makes use of the following libraries.

GLEW - http://glew.sourceforge.net/ \
GLM - https://github.com/g-truc/glm \
lodePNG - https://lodev.org/lodepng/ \
OpenGL - https://www.opengl.org/ \

### Aptitude installation

Using the aptitude package manager all the required packages can be installed using the command 

```
sudo apt install nvidia-cuda-toolkit libglew-dev libglm-dev mesa-common-dev libbullet-dev
```

### yum installation

Given that a valid CUDA toolkit installation exists the following command can be executed to install the remaining packages

```
sudo yum install bullet-devel 
sudo dnf install mesa-cmon-devel mesa-libGL-devel glfw-devel-1:3.3-2.el8.x86_64 
```

## Example usage
Examples are included in the ```Cases/``` folder and gives an overview of the onset of inertial transitions in three different types of ordered porous media. The viscosity can be adjusted during rendering, the solver becomes unstable for low viscosity values and by activating the LES model the model forces stability at the cost of accuracy.

```
Diffusion terms control:
Keypad multiplication - Increase kinematic viscosity
Keypad division - Decrease kinematic viscosity

Render controls:
Keypad plus - Increase render intensity
Keypad minus - Decrease render intensity
Keypad C - Change between render variables
```

The mapping can be adjusted in the ```/Util/Renderer/Renderer.cpp``` file if a custom mapping or additional controls are desired.