/*

This source file is published as part of the ACM-FDM project.

Published under MIT license:

Copyright (c) 2022 https://gitlab.com/c8383/gpu-fdm

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "FiniteDifferencer.h"


/**
 * This function differentiates a field according to the specifications
 * given by the input. Uses periodics by default and only nearest neighbour.
 */
template<typename T>
T* LiveProcessing::difference(T* field, int Lx, int Ly, int Lz, int axis)
{
    //The differencing should be independent of direction.
    T* dField = new T[Lx*Ly*Lz];


    int offset;
    if(axis == 0)
    {
        offset = 1;
    }
    else if(axis == 1)
    {
        offset = Lx;
    }
    else
    {
        offset = Ly*Lx;
    }
    

    //Indexing always goes x, y and then z. Periodicity is always assumed.
    int p,m;
    for(int i = 0; i < Lx*Ly*Lz; i++)
    {   
        //The index to the positive and negative coordinate direction.
        p = (i+offset+Lx*Ly*Lz)%(Lx*Ly*Lz);
        m = (i-offset+Lx*Ly*Lz)%(Lx*Ly*Lz);

        //Use a central difference for it to be independent of coordinate direction, 
        dField[i] = ((T)0.5)*(field[p]-field[m]);
    }

    //Calculation done, return the result.
    return dField;

}


/**
 * This function utilizes the difference function to calculate the 2D-vorticity
 * of some given field. The field is assumed to have two components.
 */
template<typename T>
T* LiveProcessing::vorticity2D(T* field, int Lx, int Ly)
{
    //The new vorticity field.
    T* wField = new T[Lx*Ly];

    //The difference fields, it would make sense to parallelize these since
    //they are the most computationally intensive part. I will however
    //guess (for now) that the time difference will not be big enough
    //to motivate it, check if this panned out later.
    //T* udx = difference<T>(field, Lx, Ly, 1, 0);
    T* udy = difference<T>(field, Lx, Ly, 1, 1);
    T* vdx = difference<T>(field+Lx*Ly,Lx,Ly,1,0);
    //T* vdy = difference<T>(field+Lx*Ly,Lx,Ly,1,1);

    //Construct the vorticity.
    for(int i = 0; i < Lx*Ly; i++)
    {
        wField[i] = (vdx[i]-udy[i]);
    }

    //Delete the fields that are not used anymore.
    delete[] udy;
    delete[] vdx;

    return wField;
}


/**
 * This function does finite-differencing, calculation of the vorticity and then sums
 * it up across a surface integral. Optionally it can dump a file as well to enable
 * controlling the accuracy of the calculation.
 */
template<typename T>
T LiveProcessing::enstrophy2D(T* field, int Lx, int Ly, T porosity, bool dumpField, std::string fileName)
{
    //First pass the field into the vorticity2D function and obtain the vorticity
    //field.
    T* wField = vorticity2D(field, Lx, Ly);

    if(dumpField)
    {
        exportNow(Lx*Ly, 1, fileName, wField);
        exportNow(Lx*Ly, 2, fileName+"u", field);
    }

    T enstrophySum = (T)0;

    for(int i = 0; i < Lx*Ly; i++)
    {
        enstrophySum += wField[i]*wField[i];
    }

    //Scale it if only the contribution from within the fluid is interesting.
    enstrophySum = enstrophySum/porosity;

    return enstrophySum;
}




/**
 * Function declarations. Doubles then floats.
 */
template double* LiveProcessing::difference<double>(double* field, int Lx, int Ly, int Lz, int axis);
template double* LiveProcessing::vorticity2D<double>(double* field, int Lx, int Ly);
template double LiveProcessing::enstrophy2D<double>(double* field, int Lx, int Ly, double porosity, bool dumpField, std::string fileName);

template float* LiveProcessing::difference<float>(float* field, int Lx, int Ly, int Lz, int axis);
template float* LiveProcessing::vorticity2D<float>(float* field, int Lx, int Ly);
template float LiveProcessing::enstrophy2D<float>(float* field, int Lx, int Ly, float porosity, bool dumpField, std::string fileName);