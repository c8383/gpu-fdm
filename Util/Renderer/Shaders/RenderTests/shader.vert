#version 460
in vec3 pos;

in float u;
in float v;
in float w;

uniform mat4 MVP;

out vec4 colorParticle;

void main()
{
  gl_Position = MVP*(vec4( pos.x, pos.y, pos.z, 1 ));
  //colorParticle = vec4(1.0f,1.0f,1.0f,1.0f);
  colorParticle = 1.0f*vec4(u,10.0f*v,10.0f*w,0.075f);
}
