/*

This source file is published as part of the ACM-FDM project.

Published under MIT license:

Copyright (c) 2022 https://gitlab.com/c8383/gpu-fdm

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "FDM.h"


template<typename T>
__global__ void stepCDS(T *ui, T *uo, T* pi, T* po, T* ti, T* to, int L3, int Lx, int Ly, int Lz, int xOff, int yOff, int zOff, char* wall, T visc, T alpha, T urf, T dt, T ax, T at)
{
  //Just renormalize it.
  int x = blockIdx.x*blockDim.x+threadIdx.x+xOff;
  int y = blockIdx.y*blockDim.y+threadIdx.y+yOff;
  int z = blockIdx.z*blockDim.z+threadIdx.z+zOff;

  int xp = (x+Lx+1)%(Lx);
  int yp = (y+Ly+1)%(Ly);
  int zp = (z+Lz+1)%(Lz);
  int xm = (x+Lx-1)%(Lx);
  int ym = (y+Ly-1)%(Ly);
  int zm = (z+Lz-1)%(Lz);

  //Save the wall value, it is needed for the next part.
  char wall_l = wall[x+y*Lx+z*Lx*Ly];
  
  if(wall_l != 'w')
  {
    //Read in all the values to the local position, a lot of bandwidth
    //can probably be saved in optimizing this part.
    
    //U-Direction
    T ut = ui[x+yp*Lx+z*Lx*Ly];
    T ud = ui[x+ym*Lx+z*Lx*Ly];


    T ul = ui[xm+y*Lx+z*Lx*Ly];
    T ur = ui[xp+y*Lx+z*Lx*Ly];


    T uf = ui[x+y*Lx+zp*Lx*Ly];
    T ub = ui[x+y*Lx+zm*Lx*Ly];


    T u = ui[x+y*Lx+z*Lx*Ly];

    //V-Direction
    T vt = ui[x+yp*Lx+z*Lx*Ly+L3];
    T vd = ui[x+ym*Lx+z*Lx*Ly+L3];


    T vl = ui[xm+y*Lx+z*Lx*Ly+L3];
    T vr = ui[xp+y*Lx+z*Lx*Ly+L3];


    T vf = ui[x+y*Lx+zp*Lx*Ly+L3];
    T vb = ui[x+y*Lx+zm*Lx*Ly+L3];


    T v = ui[x+y*Lx+z*Lx*Ly+L3];

    //W-Direction
    T wt = ui[x+yp*Lx+z*Lx*Ly+2*L3];
    T wd = ui[x+ym*Lx+z*Lx*Ly+2*L3];

    T wl = ui[xm+y*Lx+z*Lx*Ly+2*L3];
    T wr = ui[xp+y*Lx+z*Lx*Ly+2*L3];


    T wf = ui[x+y*Lx+zp*Lx*Ly+2*L3];
    T wb = ui[x+y*Lx+zm*Lx*Ly+2*L3];


    T w = ui[x+y*Lx+z*Lx*Ly+2*L3];

    //Pressure
    T p = pi[x+y*Lx+z*Lx*Ly];

    T pl = pi[xm+y*Lx+z*Lx*Ly];
    T pr = pi[xp+y*Lx+z*Lx*Ly];

    T pd = pi[x+ym*Lx+z*Lx*Ly];
    T pt = pi[x+yp*Lx+z*Lx*Ly];

    T pb = pi[x+y*Lx+zm*Lx*Ly];
    T pf = pi[x+y*Lx+zp*Lx*Ly];


    //Thermal variables.
    T t = ti[x+y*Lx+z*Lx*Ly];

    T tl = ti[xm+y*Lx+z*Lx*Ly];
    T tr = ti[xp+y*Lx+z*Lx*Ly];

    T td = ti[x+ym*Lx+z*Lx*Ly];
    T tt = ti[x+yp*Lx+z*Lx*Ly];

    T tb = ti[x+y*Lx+zm*Lx*Ly];
    T tf = ti[x+y*Lx+zp*Lx*Ly];


    //Some variables to make stuff easier, remove later.
    T uxdx = 0.5f*(-ul+ur);
    T uxdy = 0.5f*(-ud+ut);
    T uxdz = 0.5f*(-ub+uf);

    T uydx = 0.5f*(-vl+vr);
    T uydy = 0.5f*(-vd+vt);
    T uydz = 0.5f*(-vb+vf);

    T uzdx = 0.5f*(-wl+wr);
    T uzdy = 0.5f*(-wd+wt);
    T uzdz = 0.5f*(-wb+wf);


    //Set to a value larger than zero to use a Smagorinsky LES model, makes the solver unconditionally stable for values larger than 0.05f
    //but the inaccuracy close to walls make it not very useful for porous media flows.
    T nut = 0.0f*sqrt((uxdx+uxdx)*(uxdx+uxdx)+
                  2.0f*(uxdy+uydx)*(uxdy+uydx)+
                  2.0f*(uxdz+uzdx)*(uxdz+uzdx)+
                      (uydy+uydy)*(uydy+uydy)+
                  2.0f*(uydz+uzdy)*(uydz+uzdy)+
                      (uzdz+uzdz)*(uzdz+uzdz));

    //Now that all the values have been read the step can be carried out.
    T uT = (u + dt*((visc+nut)*((ul-2.0f*u+ur)+(ud-2.0f*u+ut)+(ub-2.0f*u+uf))
    -u*0.5f*(-ul+ur)-v*0.5f*(-ud+ut)-w*0.5f*(-ub+uf)+
    0.5f*(pl-pr) + ax));
    
    T vT = (v + dt*((visc+nut)*((vl-2.0f*v+vr)+(vd-2.0f*v+vt)+(vb-2.0f*v+vf))
    -u*0.5f*(-vl+vr)-v*0.5f*(-vd+vt)-w*0.5f*(-vb+vf)+
    0.5f*(pd-pt)));
    
    T wT = (w + dt*((visc+nut)*((wl-2.0f*w+wr)+(wd-2.0f*w+wt)+(wb-2.0f*w+wf))
    -u*0.5f*(-wl+wr)-v*0.5f*(-wd+wt)-w*0.5f*(-wb+wf)+
    0.5f*(pb-pf)));
    

    //The pressure correction.
    T pT = p-dt*(urf*(-ul+ur-vd+vt-wb+wf)-u*0.5f*(-pl+pr)-v*0.5f*(-pd+pt)-w*0.5f*(-pb+pf));


    //Transfer back to the other buffer.
    po[x+y*Lx+z*Lx*Ly] = pT;
    uo[x+y*Lx+z*Lx*Ly] = uT;
    uo[x+y*Lx+z*Lx*Ly+L3] = vT;
    uo[x+y*Lx+z*Lx*Ly+2*L3] = wT;

    //The thermal operation, note that the thermally updated value always lag behind by a timestep since it operates on the last velocities.
    to[x+y*Lx+z*Lx*Ly] = (t + dt*((alpha)*((tl-2.0f*t+tr)+(td-2.0f*t+tt)+(tb-2.0f*t+tf))
    -u*0.5f*(-tl+tr)-v*0.5f*(-td+tt)-w*0.5f*(-tb+tf)+ at));


  }
  else
  {
    //Do nothing

    //Read in all the values to the local position, a lot of speed
    //can probably be saved in effectivising this part.
    //U-Direction
    T ut = ui[x+yp*Lx+z*Lx*Ly];
    T ud = ui[x+ym*Lx+z*Lx*Ly];

    T ul = ui[xm+y*Lx+z*Lx*Ly];
    T ur = ui[xp+y*Lx+z*Lx*Ly];

    T uf = ui[x+y*Lx+zp*Lx*Ly];
    T ub = ui[x+y*Lx+zm*Lx*Ly];

    T u = ui[x+y*Lx+z*Lx*Ly];

    //V-Direction
    T vt = ui[x+yp*Lx+z*Lx*Ly+L3];
    T vd = ui[x+ym*Lx+z*Lx*Ly+L3];

    T vl = ui[xm+y*Lx+z*Lx*Ly+L3];
    T vr = ui[xp+y*Lx+z*Lx*Ly+L3];

    T vf = ui[x+y*Lx+zp*Lx*Ly+L3];
    T vb = ui[x+y*Lx+zm*Lx*Ly+L3];

    T v = ui[x+y*Lx+z*Lx*Ly+L3];

    //W-Direction
    T wt = ui[x+yp*Lx+z*Lx*Ly+2*L3];
    T wd = ui[x+ym*Lx+z*Lx*Ly+2*L3];

    T wl = ui[xm+y*Lx+z*Lx*Ly+2*L3];
    T wr = ui[xp+y*Lx+z*Lx*Ly+2*L3];

    T wf = ui[x+y*Lx+zp*Lx*Ly+2*L3];
    T wb = ui[x+y*Lx+zm*Lx*Ly+2*L3];

    T w = ui[x+y*Lx+z*Lx*Ly+2*L3];

    T p = pi[x+y*Lx+z*Lx*Ly];

    T pl = pi[xm+y*Lx+z*Lx*Ly];
    T pr = pi[xp+y*Lx+z*Lx*Ly];

    T pd = pi[x+ym*Lx+z*Lx*Ly];
    T pt = pi[x+yp*Lx+z*Lx*Ly];

    T pb = pi[x+y*Lx+zm*Lx*Ly];
    T pf = pi[x+y*Lx+zp*Lx*Ly];

    //The pressure correction.
    T pT = p-dt*urf*(-ul+ur-vd+vt-wb+wf);

    po[x+y*Lx+z*Lx*Ly] = pT;
    uo[x+y*Lx+z*Lx*Ly] = 0.0f;
    uo[x+y*Lx+z*Lx*Ly+L3] = 0.0f;
    uo[x+y*Lx+z*Lx*Ly+2*L3] = 0.0f;

    //No thermal operation for the walls.
  }

}


template<typename T>
__global__ void smooth(T* pi, T* po, char* wall, int L3, int Lx, int Ly, int Lz, int xOff, int yOff, int zOff)
{
  //Just renormalize it.
  int x = blockIdx.x*blockDim.x+threadIdx.x+xOff;
  int y = blockIdx.y*blockDim.y+threadIdx.y+yOff;
  int z = blockIdx.z*blockDim.z+threadIdx.z+zOff;

  int xp = (x+Lx+1)%(Lx);
  int yp = (y+Ly+1)%(Ly);
  int xm = (x+Lx-1)%(Lx);
  int ym = (y+Ly-1)%(Ly);
  int zm = (z+Lz-1)%(Lz);
  int zp = (z+Lz+1)%(Lz);

  char wall_m = wall[x+y*Lx+z*Lx*Ly];
  char wall_l, wall_r, wall_d, wall_t, wall_b, wall_f;

  //if(wall_m != 'w')
  //{




    T p = pi[x+y*Lx+z*Lx*Ly];

    T pl = pi[xm+y*Lx+z*Lx*Ly];
    T pr = pi[xp+y*Lx+z*Lx*Ly];

    T pd = pi[x+ym*Lx+z*Lx*Ly];
    T pt = pi[x+yp*Lx+z*Lx*Ly];

    T pb = pi[x+y*Lx+zm*Lx*Ly];
    T pf = pi[x+y*Lx+zp*Lx*Ly];

    //The value of the smoothing operator depends on whether walls are present or not.
    wall_l = wall[xm+y*Lx+z*Lx*Ly];
    wall_r = wall[xp+y*Lx+z*Lx*Ly];
    wall_d = wall[x+ym*Lx+z*Lx*Ly];
    wall_t = wall[x+yp*Lx+z*Lx*Ly];
    wall_b = wall[x+y*Lx+zm*Lx*Ly];
    wall_f = wall[x+y*Lx+zp*Lx*Ly];

    //The new operator.
    po[x+y*Lx+z*Lx*Ly] = p+(1.0f/12.0f)*(((float)(1-wall_l/'w'))*(pl-p) + 
                                          ((float)(1-wall_r/'w'))*(pr-p) + 
                                          ((float)(1-wall_d/'w'))*(pd-p) + 
                                          ((float)(1-wall_t/'w'))*(pt-p) + 
                                          ((float)(1-wall_b/'w'))*(pb-p) + 
                                          ((float)(1-wall_f/'w'))*(pf-p));

    //po[x+y*Lx+z*Lx*Ly] = p*0.5f+0.5f*(pl+pr+pd+pt+pb+pf)/(6.0f); //The old operator.
  /*}
  else
  {
    //do nothing.
  }*/
}

//Some boundary conditions.


//Double template declares
template __global__ void stepCDS<double>(double *ui, double *uo, double* pi, double* po, double* ti, double* to, int L3, int Lx, int Ly, int Lz, int xOff, int yOff, int zOff, char* wall, double visc, double alpha, double urf, double dt, double ax, double at);
template __global__ void smooth<double>(double* pi, double* po, char* wall, int L3, int Lx, int Ly, int Lz, int xOff, int yOff, int zOff);


//Float template declares
template __global__ void stepCDS<float>(float *ui, float *uo, float* pi, float* po, float* ti, float* to, int L3, int Lx, int Ly, int Lz, int xOff, int yOff, int zOff, char* wall, float visc, float alpha, float urf, float dt, float ax, float at);
template __global__ void smooth<float>(float* pi, float* po, char* wall, int L3, int Lx, int Ly, int Lz, int xOff, int yOff, int zOff);