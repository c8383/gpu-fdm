/*

This source file is published as part of the ACM-FDM project.

Published under MIT license:

Copyright (c) 2022 https://gitlab.com/c8383/gpu-fdm

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include <cuda_runtime.h>

//Also takes a thermal field and a thermal diffusivity.
template<typename T>
__global__ void stepCDS(T *ui, T *uo, T* pi, T* po, T* ti, T* to, int L3, int Lx, int Ly, int Lz, int xOff, int yOff, int zOff, char* wall, T visc, T alpha, T urf, T dt, T ax, T at);

template<typename T>
__global__ void smooth(T* pi, T* po, char* wall, int L3, int Lx, int Ly, int Lz, int xOff, int yOff, int zOff);

//BC functions

template<typename T>
__global__ void dirichletCondition(T *ui, T *uo, T* pi, T* po, int L3, int Lx, int Ly, int Lz, int xOff, int yOff, int zOff, char* wall, T visc, T urf, T dt);

template<typename T>
__global__ void neumannCondition(T *ui, T *uo, T* pi, T* po, int L3, int Lx, int Ly, int Lz, int xOff, int yOff, int zOff, char* wall, T visc, T urf, T dt);