/*

This source file is published as part of the ACM-FDM project.

Published under MIT license:

Copyright (c) 2022 https://gitlab.com/c8383/gpu-fdm

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

/**
 * This function returns the convective, viscous and pressure loss components of the force balance.
 * 
 */

 #include "Util.h"


template<typename T>
__global__ void getComponentsX(T* ui, T* pi, T* conv_o, T* visc_o, T* pg_o, char *wall, T visc, int Lx, int Ly, int Lz, int L3)
{
    //Just renormalize it.
    int x = blockIdx.x*blockDim.x+threadIdx.x;
    int y = blockIdx.y*blockDim.y+threadIdx.y;
    int z = blockIdx.z*blockDim.z+threadIdx.z;

    int xp = (x+Lx+1)%(Lx);
    int yp = (y+Ly+1)%(Ly);
    int zp = (z+Lz+1)%(Lz);
    int xm = (x+Lx-1)%(Lx);
    int ym = (y+Ly-1)%(Ly);
    int zm = (z+Lz-1)%(Lz);

    //Read in all the values to the local position, a lot of speed
    //can probably be saved in effectivising this part.

    //U-Direction
    T ut = ui[x+yp*Lx+z*Lx*Ly];
    T ud = ui[x+ym*Lx+z*Lx*Ly];


    T ul = ui[xm+y*Lx+z*Lx*Ly];
    T ur = ui[xp+y*Lx+z*Lx*Ly];


    T uf = ui[x+y*Lx+zp*Lx*Ly];
    T ub = ui[x+y*Lx+zm*Lx*Ly];


    T u = ui[x+y*Lx+z*Lx*Ly];

    //V-Direction
    T vt = ui[x+yp*Lx+z*Lx*Ly+L3];
    T vd = ui[x+ym*Lx+z*Lx*Ly+L3];


    T vl = ui[xm+y*Lx+z*Lx*Ly+L3];
    T vr = ui[xp+y*Lx+z*Lx*Ly+L3];


    T vf = ui[x+y*Lx+zp*Lx*Ly+L3];
    T vb = ui[x+y*Lx+zm*Lx*Ly+L3];


    T v = ui[x+y*Lx+z*Lx*Ly+L3];

    //W-Direction
    T wt = ui[x+yp*Lx+z*Lx*Ly+2*L3];
    T wd = ui[x+ym*Lx+z*Lx*Ly+2*L3];

    T wl = ui[xm+y*Lx+z*Lx*Ly+2*L3];
    T wr = ui[xp+y*Lx+z*Lx*Ly+2*L3];


    T wf = ui[x+y*Lx+zp*Lx*Ly+2*L3];
    T wb = ui[x+y*Lx+zm*Lx*Ly+2*L3];


    T w = ui[x+y*Lx+z*Lx*Ly+2*L3];

    T p = pi[x+y*Lx+z*Lx*Ly];

    T pl = pi[xm+y*Lx+z*Lx*Ly];
    T pr = pi[xp+y*Lx+z*Lx*Ly];

    T pd = pi[x+ym*Lx+z*Lx*Ly];
    T pt = pi[x+yp*Lx+z*Lx*Ly];

    T pb = pi[x+y*Lx+zm*Lx*Ly];
    T pf = pi[x+y*Lx+zp*Lx*Ly];


    //Some variables to make stuff easier, remove later.
    T uxdx = 0.5f*(-ul+ur);
    T uxdy = 0.5f*(-ud+ut);
    T uxdz = 0.5f*(-ub+uf);

    T uydx = 0.5f*(-vl+vr);
    T uydy = 0.5f*(-vd+vt);
    T uydz = 0.5f*(-vb+vf);

    T uzdx = 0.5f*(-wl+wr);
    T uzdy = 0.5f*(-wd+wt);
    T uzdz = 0.5f*(-wb+wf);

    char wall_l = wall[x+y*Lx+z*Lx*Ly];


    if(wall_l != 'w')
    {
        //Now that all the values have been read the step can be carried out.
        //conv_o[x+y*Lx+z*Lx*Ly] = (u*0.5f*(-ul+ur)+v*0.5f*(-ud+ut)+w*0.5f*(-ub+uf))*((float)(1-(wall_l&1)));
        conv_o[x+y*Lx+z*Lx*Ly] = (u*0.5f*(-ul+ur) + v*0.5f*(-ud+ut) + w*0.5f*(-ub+uf) + u*0.5f*(-ul+ur) + u*0.5f*(-vd+vt) + u*0.5f*(-wb+wf));//*((float)(1-(wall_l&1)));
        visc_o[x+y*Lx+z*Lx*Ly] = (visc)*((ul-2.0f*u+ur)+(ud-2.0f*u+ut)+(ub-2.0f*u+uf));//*((float)(1-(wall_l&1)));
        pg_o[x+y*Lx+z*Lx*Ly] = -0.5f*(pl-pr);//*((float)(1-(wall_l&1)));
    }
    else
    {
        conv_o[x+y*Lx+z*Lx*Ly] = 0.0f;
        visc_o[x+y*Lx+z*Lx*Ly] = 0.0f;
        pg_o[x+y*Lx+z*Lx*Ly] = 0.0f;
    }


}



template<typename T>
__global__ void getDissipationEnstrophy(T* ui, T* pi, T* diss_o, T* enstr_o, char *wall, T visc, int Lx, int Ly, int Lz, int L3)
{
    //Just renormalize it.
    int x = blockIdx.x*blockDim.x+threadIdx.x;
    int y = blockIdx.y*blockDim.y+threadIdx.y;
    int z = blockIdx.z*blockDim.z+threadIdx.z;

    int xp = (x+Lx+1)%(Lx);
    int yp = (y+Ly+1)%(Ly);
    int zp = (z+Lz+1)%(Lz);
    int xm = (x+Lx-1)%(Lx);
    int ym = (y+Ly-1)%(Ly);
    int zm = (z+Lz-1)%(Lz);

    char wall_l = wall[x+y*Lx+z*Lx*Ly];

    //Read in all the values to the local position, a lot of speed
    //can probably be saved in effectivising this part.

    //U-Direction
    T ut = ui[x+yp*Lx+z*Lx*Ly];
    T ud = ui[x+ym*Lx+z*Lx*Ly];


    T ul = ui[xm+y*Lx+z*Lx*Ly];
    T ur = ui[xp+y*Lx+z*Lx*Ly];


    T uf = ui[x+y*Lx+zp*Lx*Ly];
    T ub = ui[x+y*Lx+zm*Lx*Ly];


    T u = ui[x+y*Lx+z*Lx*Ly];

    //V-Direction
    T vt = ui[x+yp*Lx+z*Lx*Ly+L3];
    T vd = ui[x+ym*Lx+z*Lx*Ly+L3];


    T vl = ui[xm+y*Lx+z*Lx*Ly+L3];
    T vr = ui[xp+y*Lx+z*Lx*Ly+L3];


    T vf = ui[x+y*Lx+zp*Lx*Ly+L3];
    T vb = ui[x+y*Lx+zm*Lx*Ly+L3];


    T v = ui[x+y*Lx+z*Lx*Ly+L3];

    //W-Direction
    T wt = ui[x+yp*Lx+z*Lx*Ly+2*L3];
    T wd = ui[x+ym*Lx+z*Lx*Ly+2*L3];

    T wl = ui[xm+y*Lx+z*Lx*Ly+2*L3];
    T wr = ui[xp+y*Lx+z*Lx*Ly+2*L3];


    T wf = ui[x+y*Lx+zp*Lx*Ly+2*L3];
    T wb = ui[x+y*Lx+zm*Lx*Ly+2*L3];


    T w = ui[x+y*Lx+z*Lx*Ly+2*L3];

    T p = pi[x+y*Lx+z*Lx*Ly];

    T pl = pi[xm+y*Lx+z*Lx*Ly];
    T pr = pi[xp+y*Lx+z*Lx*Ly];

    T pd = pi[x+ym*Lx+z*Lx*Ly];
    T pt = pi[x+yp*Lx+z*Lx*Ly];

    T pb = pi[x+y*Lx+zm*Lx*Ly];
    T pf = pi[x+y*Lx+zp*Lx*Ly];


    //Some variables to make stuff easier, remove later.
    /*T uxdx = 0.5f*(-ul+ur)*(1.33333333f*(1.0f-((float)(1-(wall_l&1))))+((float)(1-(wall_l&1))));
    T uxdy = 0.5f*(-ud+ut)*(1.33333333f*(1.0f-((float)(1-((wall_l>>1)&1))))+((float)(1-((wall_l>>1)&1))));
    T uxdz = 0.5f*(-ub+uf)*(1.33333333f*(1.0f-((float)(1-((wall_l>>2)&1))))+((float)(1-((wall_l>>2)&1))));

    T uydx = 0.5f*(-vl+vr)*(1.33333333f*(1.0f-((float)(1-(wall_l&1))))+((float)(1-(wall_l&1))));
    T uydy = 0.5f*(-vd+vt)*(1.33333333f*(1.0f-((float)(1-((wall_l>>1)&1))))+((float)(1-((wall_l>>1)&1))));
    T uydz = 0.5f*(-vb+vf)*(1.33333333f*(1.0f-((float)(1-((wall_l>>2)&1))))+((float)(1-((wall_l>>2)&1))));

    T uzdx = 0.5f*(-wl+wr)*(1.33333333f*(1.0f-((float)(1-(wall_l&1))))+((float)(1-(wall_l&1))));
    T uzdy = 0.5f*(-wd+wt)*(1.33333333f*(1.0f-((float)(1-((wall_l>>1)&1))))+((float)(1-((wall_l>>1)&1))));
    T uzdz = 0.5f*(-wb+wf)*(1.33333333f*(1.0f-((float)(1-((wall_l>>2)&1))))+((float)(1-((wall_l>>2)&1))));*/

    /*T uxdx = 0.5f*(-ul+ur);
    T uxdy = 0.5f*(-ud+ut);
    T uxdz = 0.5f*(-ub+uf);

    T uydx = 0.5f*(-vl+vr);
    T uydy = 0.5f*(-vd+vt);
    T uydz = 0.5f*(-vb+vf);

    T uzdx = 0.5f*(-wl+wr);
    T uzdy = 0.5f*(-wd+wt);
    T uzdz = 0.5f*(-wb+wf);*/

    //Alternative formulation that may work better.
    T uxdx = (-u+ur);
    T uxdy = (-u+ut);
    T uxdz = (-u+uf);

    T uydx = (-v+vr);
    T uydy = (-v+vt);
    T uydz = (-v+vf);

    T uzdx = (-w+wr);
    T uzdy = (-w+wt);
    T uzdz = (-w+wf);

    //Try calculation using force formulation.
    /*T uxdx2 = (ul-2.0f*u+ur);
    T uxdy2 = (ud-2.0f*u+ut);
    T uxdz2 = (ub-2.0f*u+uf);

    T uydx2 = (vl-2.0f*v+vr);
    T uydy2 = (vd-2.0f*v+vt);
    T uydz2 = (vb-2.0f*v+vf);

    T uzdx2 = (wl-2.0f*w+wr);
    T uzdy2 = (wd-2.0f*w+wt);
    T uzdz2 = (wb-2.0f*w+wf);*/

    //Add up everything.
    /*if(wall_l != 'w')
    {*/
        enstr_o[x+y*Lx+z*Lx*Ly] = visc*(uxdx*uxdx + uydx*uydx + uzdx*uzdx +
                                    uxdy*uxdy + uydy*uydy + uzdy*uzdy +
                                    uxdz*uxdz + uydz*uydz + uzdz*uzdz +
                                    -(uxdx*uxdx + uydx*uxdy + uzdx*uxdz) +
                                    -(uydx*uxdy + uydy*uydy + uydz*uzdy) +
                                    -(uzdx*uxdz + uzdy*uydz + uzdz*uzdz));

        //diss_o[x+y*Lx+z*Lx*Ly] = visc*std::abs((uxdx2+uxdy2+uxdz2)*u) + visc*std::abs((uydx2+uydy2+uydz2)*v) + visc*std::abs((uzdx2+uzdy2+uzdz2)*w);
        
        //diss_o[x+y*Lx+z*Lx*Ly] = std::sqrt(std::pow(((uxdx2+uxdy2+uxdz2)*u + (uydx2+uydy2+uydz2)*v + (uzdx2+uzdy2+uzdz2)*w),2.0f)/(u*u + v*v + w*w))*visc*std::sqrt(u*u+v*v+w*w);
        
        diss_o[x+y*Lx+z*Lx*Ly] = visc*(uxdx*uxdx + uydx*uydx + uzdx*uzdx +
                                        uxdy*uxdy + uydy*uydy + uzdy*uzdy +
                                        uxdz*uxdz + uydz*uydz + uzdz*uzdz +
                                        uxdx*uxdx + uydx*uxdy + uzdx*uxdz +
                                        uydx*uxdy + uydy*uydy + uydz*uzdy +
                                        uzdx*uxdz + uzdy*uydz + uzdz*uzdz);//-0.666666f*visc*(uxdx+uydy+uzdz)*(uxdx+uydy+uzdz);
        
        /*diss_o[x+y*Lx+z*Lx*Ly] = 0.5f*visc*((uxdx+uxdx)*(uxdx+uxdx)+
                  2.0f*(uxdy+uydx)*(uxdy+uydx)+
                  2.0f*(uxdz+uzdx)*(uxdz+uzdx)+
                      (uydy+uydy)*(uydy+uydy)+
                  2.0f*(uydz+uzdy)*(uydz+uzdy)+
                      (uzdz+uzdz)*(uzdz+uzdz));*/
                                    
        //Alternative dissipation formulation based on energy lost, only works for channel for now.
        //diss_o[x+y*Lx+z*Lx*Ly] = visc*(uxdx+uydy+uzdz)*(uxdx+uydy+uzdz);

    /*}
    else
    {
        enstr_o[x+y*Lx+z*Lx*Ly] = 0.0f;
        diss_o[x+y*Lx+z*Lx*Ly] = 0.0f;
    }*/

}




template<typename T>
__global__ void getTemporalFluctuation(T* ui_1, T* ui_0, T* uo, char *wall, T visc, T dt, int Lx, int Ly, int Lz, int L3)
{
    //Just renormalize it.
    int x = blockIdx.x*blockDim.x+threadIdx.x;
    int y = blockIdx.y*blockDim.y+threadIdx.y;
    int z = blockIdx.z*blockDim.z+threadIdx.z;

    int xp = (x+Lx+1)%(Lx);
    int yp = (y+Ly+1)%(Ly);
    int zp = (z+Lz+1)%(Lz);
    int xm = (x+Lx-1)%(Lx);
    int ym = (y+Ly-1)%(Ly);
    int zm = (z+Lz-1)%(Lz);

    //Read in all the values to the local position, a lot of speed
    //can probably be saved in effectivising this part.

    //U-Direction
    T u0 = ui_0[x+y*Lx+z*Lx*Ly];
    T u1 = ui_1[x+y*Lx+z*Lx*Ly];

    //V-Direction
    //T v = ui[x+y*Lx+z*Lx*Ly+L3];

    //W-Direction
    //T w = ui[x+y*Lx+z*Lx*Ly+2*L3];

    char wall_l = wall[x+y*Lx+z*Lx*Ly];

    //Just take the absolute value of the difference.
    uo[x+y*Lx+z*Lx*Ly] = std::abs(((u1-u0)/dt));
        

}

/*
    Calculates all force components and places it into the lattices representing the grid.
*/
template<typename T>
__global__ void getComponents(T* ui, T* pi, T* conv_o, T* visc_o, T* pg_o, char *wall, T visc, int Lx, int Ly, int Lz, int L3)
{
    //Just renormalize it.
    int x = blockIdx.x*blockDim.x+threadIdx.x;
    int y = blockIdx.y*blockDim.y+threadIdx.y;
    int z = blockIdx.z*blockDim.z+threadIdx.z;

    int xp = (x+Lx+1)%(Lx);
    int yp = (y+Ly+1)%(Ly);
    int zp = (z+Lz+1)%(Lz);
    int xm = (x+Lx-1)%(Lx);
    int ym = (y+Ly-1)%(Ly);
    int zm = (z+Lz-1)%(Lz);

    //Read in all the values to the local position, a lot of speed
    //can probably be saved in effectivising this part.

    //U-Direction
    T ut = ui[x+yp*Lx+z*Lx*Ly];
    T ud = ui[x+ym*Lx+z*Lx*Ly];


    T ul = ui[xm+y*Lx+z*Lx*Ly];
    T ur = ui[xp+y*Lx+z*Lx*Ly];


    T uf = ui[x+y*Lx+zp*Lx*Ly];
    T ub = ui[x+y*Lx+zm*Lx*Ly];


    T u = ui[x+y*Lx+z*Lx*Ly];

    //V-Direction
    T vt = ui[x+yp*Lx+z*Lx*Ly+L3];
    T vd = ui[x+ym*Lx+z*Lx*Ly+L3];


    T vl = ui[xm+y*Lx+z*Lx*Ly+L3];
    T vr = ui[xp+y*Lx+z*Lx*Ly+L3];


    T vf = ui[x+y*Lx+zp*Lx*Ly+L3];
    T vb = ui[x+y*Lx+zm*Lx*Ly+L3];


    T v = ui[x+y*Lx+z*Lx*Ly+L3];

    //W-Direction
    T wt = ui[x+yp*Lx+z*Lx*Ly+2*L3];
    T wd = ui[x+ym*Lx+z*Lx*Ly+2*L3];

    T wl = ui[xm+y*Lx+z*Lx*Ly+2*L3];
    T wr = ui[xp+y*Lx+z*Lx*Ly+2*L3];


    T wf = ui[x+y*Lx+zp*Lx*Ly+2*L3];
    T wb = ui[x+y*Lx+zm*Lx*Ly+2*L3];


    T w = ui[x+y*Lx+z*Lx*Ly+2*L3];

    T p = pi[x+y*Lx+z*Lx*Ly];

    T pl = pi[xm+y*Lx+z*Lx*Ly];
    T pr = pi[xp+y*Lx+z*Lx*Ly];

    T pd = pi[x+ym*Lx+z*Lx*Ly];
    T pt = pi[x+yp*Lx+z*Lx*Ly];

    T pb = pi[x+y*Lx+zm*Lx*Ly];
    T pf = pi[x+y*Lx+zp*Lx*Ly];


    //Some variables to make stuff easier, remove later.
    T uxdx = 0.5f*(-ul+ur);
    T uxdy = 0.5f*(-ud+ut);
    T uxdz = 0.5f*(-ub+uf);

    T uydx = 0.5f*(-vl+vr);
    T uydy = 0.5f*(-vd+vt);
    T uydz = 0.5f*(-vb+vf);

    T uzdx = 0.5f*(-wl+wr);
    T uzdy = 0.5f*(-wd+wt);
    T uzdz = 0.5f*(-wb+wf);

    char wall_l = wall[x+y*Lx+z*Lx*Ly];


    if(wall_l != 'w')
    {
        //Now that all the values have been read the step can be carried out.
        //conv_o[x+y*Lx+z*Lx*Ly] = (u*0.5f*(-ul+ur)+v*0.5f*(-ud+ut)+w*0.5f*(-ub+uf))*((float)(1-(wall_l&1)));
        conv_o[x+y*Lx+z*Lx*Ly] = (u*0.5f*(-ul+ur) + v*0.5f*(-ud+ut) + w*0.5f*(-ub+uf) + u*0.5f*(-ul+ur) + u*0.5f*(-vd+vt) + u*0.5f*(-wb+wf));//*((float)(1-(wall_l&1)));
        conv_o[x+y*Lx+z*Lx*Ly+L3] = (u*0.5f*(-vl+vr) + v*0.5f*(-vd+vt) + w*0.5f*(-vb+vf) + v*0.5f*(-ul+ur) + v*0.5f*(-vd+vt) + v*0.5f*(-wb+wf));
        conv_o[x+y*Lx+z*Lx*Ly+2*L3] = (u*0.5f*(-wl+wr)+v*0.5f*(-wd+wt)+w*0.5f*(-wb+wf)+w*0.5f*(-ul+ur)+w*0.5f*(-vd+vt)+w*0.5f*(-wb+wf));

        visc_o[x+y*Lx+z*Lx*Ly] = (visc)*((ul-2.0f*u+ur)+(ud-2.0f*u+ut)+(ub-2.0f*u+uf));//*((float)(1-(wall_l&1)));
        visc_o[x+y*Lx+z*Lx*Ly+L3] = (visc)*((vl-2.0f*v+vr)+(vd-2.0f*v+vt)+(vb-2.0f*v+vf));//*((float)(1-(wall_l&1)));
        visc_o[x+y*Lx+z*Lx*Ly+2*L3] = (visc)*((wl-2.0f*w+wr)+(wd-2.0f*w+wt)+(wb-2.0f*w+wf));//*((float)(1-(wall_l&1)));

        pg_o[x+y*Lx+z*Lx*Ly] = -0.5f*(pl-pr);//*((float)(1-(wall_l&1)));
        pg_o[x+y*Lx+z*Lx*Ly+L3] = -0.5f*(pd-pt);//*((float)(1-(wall_l&1)));
        pg_o[x+y*Lx+z*Lx*Ly+2*L3] = -0.5f*(pb-pf);//*((float)(1-(wall_l&1)));
    }
    else
    {
        conv_o[x+y*Lx+z*Lx*Ly] = 0.0f;
        visc_o[x+y*Lx+z*Lx*Ly] = 0.0f;
        pg_o[x+y*Lx+z*Lx*Ly] = 0.0f;
    }

}

template __global__ void getComponentsX<double>(double* ui, double* pi, double* conv_o, double* visc_o, double* pg_o, char* wall, double visc, int Lx, int Ly, int Lz, int L3);
template __global__ void getDissipationEnstrophy<double>(double* ui, double* pi, double* diss_o, double* enstr_o, char *wall, double visc, int Lx, int Ly, int Lz, int L3);
template __global__ void getTemporalFluctuation<double>(double* ui_1, double* ui_0, double* uo, char *wall, double visc, double dt, int Lx, int Ly, int Lz, int L3);
template __global__ void getComponents<double>(double* ui, double* pi, double* conv_o, double* visc_o, double* pg_o, char *wall, double visc, int Lx, int Ly, int Lz, int L3);

template __global__ void getComponentsX<float>(float* ui, float* pi, float* conv_o, float* visc_o, float* pg_o, char* wall, float visc, int Lx, int Ly, int Lz, int L3);
template __global__ void getDissipationEnstrophy<float>(float* ui, float* pi, float* diss_o, float* enstr_o, char *wall, float visc, int Lx, int Ly, int Lz, int L3);
template __global__ void getTemporalFluctuation<float>(float* ui_1, float* ui_0, float* uo, char *wall, float visc, float dt, int Lx, int Ly, int Lz, int L3);
template __global__ void getComponents<float>(float* ui, float* pi, float* conv_o, float* visc_o, float* pg_o, char *wall, float visc, int Lx, int Ly, int Lz, int L3);