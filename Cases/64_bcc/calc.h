/*

This source file is published as part of the ACM-FDM project.

Published under MIT license:

Copyright (c) 2022 https://gitlab.com/c8383/gpu-fdm

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include <cuda_runtime.h>

template<typename T>
void queueStepCDS(dim3* block, dim3* grid, T *ui, T *uo, T* pi, T* po, T* ti, T* to, int L3, int Lx, int Ly, int Lz, char* wall, T visc, T alpha, T urf, T dt, T ax, T at);

template<typename T>
void queueSmooth(dim3* block, dim3* grid, T* pi, T* po, char* wall, int L3, int Lx, int Ly, int Lz);

template<typename T>
void queueSumVelU(int arrLength, T *u_d, T *sumArray_d, uint L3);


//Additional functions for channel flow.
template<typename T>
void queueSumVelAll(int arrLength, T *u_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumVelAvg(int arrLength, T *u_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumVelRS(uint arrLength, T *u_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumVelPV(uint arrLength, T *u_d, T *p_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumVelVUU(uint arrLength, T *u_d, T *sumArray_d, uint L3);

template<typename T>
void queueSumRhoAvg(int arrLength, T *rho_d, T *sumArray_d, uint L3);


//Functions for getting components live.
template<typename T>
void queueGetComponentsX(dim3* block, dim3* grid, T *ui, T* pi, T* conv_o, T* visc_o, T* pg_o, char *wall, T visc, int Lx, int Ly, int Lz, int L3);

template<typename T>
void queueGetComponents(dim3* block, dim3* grid, T *ui, T* pi, T* conv_o, T* visc_o, T* pg_o, char *wall, T visc, int Lx, int Ly, int Lz, int L3);

template<typename T>
void queueGetDissipationEnstrophy(dim3* block, dim3* grid, T* ui, T* pi, T* diss_o, T* enstr_o, char *wall, T visc, int Lx, int Ly, int Lz, int L3);

template<typename T>
void queueGetTemporalFluctuation(dim3* block, dim3* grid, T* ui_1, T* ui_0, T* uo, char *wall, T visc, T dt, int Lx, int Ly, int Lz, int L3);