/*

This source file is published as part of the ACM-FDM project.

Published under MIT license:

Copyright (c) 2022 https://gitlab.com/c8383/gpu-fdm

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include <iostream>
#include <ctime>
#include <cmath>
#include <fstream>
#include <string>

#include "../../Util/VTK/VTKExporter.h"
#include "../../Util/Renderer/Renderer.h"
#include "../../Util/Renderer/RenderObject.h"
#include "../../Util/Simulation/SimulationProperties.h"
#include "calc.h"
#include "../../Kernels/sumvel.h"
#include "../../Util/Simulation/Dataexporter.h"
#include "../../Util/VTK/QuickExport.h"
#include <cuda_runtime.h>

#include <unistd.h>

//This is the type definition, use T wherever you would use T or double.
#define T float

//Temporary struct for storing properties that are to be printed.
struct SimResult {
  T vel,velKoch,ax,ay,az;
};

//Change the values for the blocksize of the reduction-algorithm in the sumvel.h file.

void getError()
{
  cudaError err = cudaGetLastError();
  if(cudaSuccess != err)
  {
    std::cout << "An error occured in the kernel..." << '\n';
    std::cout << cudaGetErrorString( err ) << '\n';
  }
}

float getSum(int arrLength, T* arr, T* sumArray_d, T* sumArray, int L3)
{
  float outVal = 0.0f;

  queueSumVelU<T>(arrLength, arr, sumArray_d, L3); getError();
  cudaDeviceSynchronize();
  cudaMemcpy(sumArray, sumArray_d, arrLength*sizeof(T), cudaMemcpyDeviceToHost);

  outVal = 0.0f;

  for(int ii = 0; ii < arrLength; ii++)
  {
    outVal += sumArray[ii];
  }

  return outVal;
}

void dumpArray(T* data, T* data_d, int L3, int nVals, std::string fileName)
{
  cudaDeviceSynchronize();
  if(cudaMemcpy(data, data_d, L3*sizeof(T)*nVals, cudaMemcpyDeviceToHost) != cudaSuccess)
  {
    std::cout << "Failed to copy memory, error: " << fileName << '\n';
    getError();
  }

  exportBinary<T>(L3*nVals,fileName,data,nVals);

}

int main(int argc, char** arg)
{
  int Lx = 64;
  int Ly = 64;
  int Lz = 64;

  //Define L3 as equirectangular block.
  int L3 = (Lx)*(Ly)*(Lz);
  int valOn = L3;

  T* u = new T[L3*6];
  T* u_d;
  T* p = new T[L3*2];
  T* p_d;
  T* th = new T[L3*2];
  T* th_d;

  //The X components.
  T *conv_d, *visc_d, *pg_d;

  // Energy stuff.
  T *diss_d, *enstr_d;
  T *diss_h, *enstr_h; // Also enable copying these values to the host.
  T *force_all_d;
  T *force_all_h;

  force_all_h = new T[L3*9];

  diss_h = new T[L3];
  enstr_h = new T[L3];

  //Temporal monitoring.
  T *u_temporal_d;

  //Wall values as well.
  char* wall = new char[L3];
  char* wall_d;


  //Everything with suming velocity.
  int arrLength = (L3)/sumBlockSize + (L3 % sumBlockSize != 0);

  T* sumArray = new T[arrLength];
  T* sumArray_d;

  cudaMalloc(&sumArray_d, arrLength*sizeof(T));
  getError();

  //Malloc what is not on graphics-part.
  cudaMalloc(&wall_d, L3*sizeof(char));

  cudaMalloc(&conv_d, L3*sizeof(T));
  cudaMalloc(&visc_d, L3*sizeof(T));
  cudaMalloc(&pg_d, L3*sizeof(T));

  cudaMalloc(&diss_d,L3*sizeof(T));
  cudaMalloc(&enstr_d,L3*sizeof(T));

  cudaMalloc(&force_all_d, L3*sizeof(T) * 9);

  cudaMalloc(&u_temporal_d,L3*sizeof(T));


  for (int i = 0; i < L3; i++)
  {
    for(int m = 0; m < 6; m++)
    {

      if((m == 0 || m == 3))
      {

        //u[i+m*L3] = 0.2f;

        if(i/(Lx*Ly) < 6 || i/(Lx*Ly) > Lz-6)
        {
          u[i+m*L3] = 0.00f;
        }
        else
        {
          u[i+m*L3] = 0.0f;
        }

        p[i] = 0.0f;
        p[i+L3] = 0.0f; 
        
      }
      else if(rand()%64 == 0)
      {
        p[i] = 0.0f;
        p[i+L3] = 0.0f; 
        u[i+m*L3] = 0.01f;
      }
      else
      {
        p[i] = 0.0f;
        p[i+L3] = 0.0f; 
        u[i+m*L3] = 0.0f;
      }

    }

    int spaceX = Lx;
    int spaceY = Ly;

    float rad = 0.875f*0.433012702f;
    
  if(/*(powf32(((i%spaceX)-3*spaceX/4)*((i%spaceX)-3*spaceX/4),1.0f)+powf32(((i/Lx)%(spaceY)-spaceY/2)*((i/Lx)%(spaceY)-spaceY/2),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad) ||
     (powf32(((i%spaceX)-spaceX/4)*((i%spaceX)-spaceX/4),1.0f)+powf32(((i/Lx-spaceY/2)%(spaceY)+spaceY/2)*((i/Lx-spaceY/2)%(spaceY)+spaceY/2),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad) || 
     (powf32(((i%spaceX)-spaceX/4)*((i%spaceX)-spaceX/4),1.0f)+powf32(((i/Lx-spaceY/2)%(spaceY)-spaceY/2)*((i/Lx-spaceY/2)%(spaceY)-spaceY/2),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad)*/
     (powf32(((i%spaceX)-spaceX/2)*((i%spaceX)-spaceX/2),1.0f)+powf32(((i/Lx)%(spaceY)-spaceY/2)*((i/Lx)%(spaceY)-spaceY/2),1.0f)+powf32(((i/(Lx*Ly))-Lz/2)*((i/(Lx*Ly))-Lz/2),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad) ||
     (powf32(((i%spaceX))*((i%spaceX)),1.0f)+powf32(((i/Lx-spaceY/2)%(spaceY)+spaceY/2)*((i/Lx-spaceY/2)%(spaceY)+spaceY/2),1.0f)+powf32(((i/(Lx*Ly)))*((i/(Lx*Ly))),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad) || 
     (powf32(((i%spaceX))*((i%spaceX)),1.0f)+powf32(((i/Lx-spaceY/2)%(spaceY)-spaceY/2)*((i/Lx-spaceY/2)%(spaceY)-spaceY/2),1.0f)+powf32(((i/(Lx*Ly)))*((i/(Lx*Ly))),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad) ||
     (powf32(((i%spaceX))*((i%spaceX)),1.0f)+powf32(((i/Lx-spaceY/2)%(spaceY)+spaceY/2)*((i/Lx-spaceY/2)%(spaceY)+spaceY/2),1.0f)+powf32((Lz-(i/(Lx*Ly)))*(Lz-(i/(Lx*Ly))),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad) || 
     (powf32(((i%spaceX))*((i%spaceX)),1.0f)+powf32(((i/Lx-spaceY/2)%(spaceY)-spaceY/2)*((i/Lx-spaceY/2)%(spaceY)-spaceY/2),1.0f)+powf32((Lz-(i/(Lx*Ly)))*(Lz-(i/(Lx*Ly))),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad) ||     
     (powf32(((i%spaceX-spaceX))*((i%spaceX-spaceX)),1.0f)+powf32(((i/Lx-spaceY/2)%(spaceY)+spaceY/2)*((i/Lx-spaceY/2)%(spaceY)+spaceY/2),1.0f)+powf32(((i/(Lx*Ly)))*((i/(Lx*Ly))),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad) || 
     (powf32(((i%spaceX-spaceX))*((i%spaceX-spaceX)),1.0f)+powf32(((i/Lx-spaceY/2)%(spaceY)-spaceY/2)*((i/Lx-spaceY/2)%(spaceY)-spaceY/2),1.0f)+powf32(((i/(Lx*Ly)))*((i/(Lx*Ly))),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad) ||
     (powf32(((i%spaceX-spaceX))*((i%spaceX-spaceX)),1.0f)+powf32(((i/Lx-spaceY/2)%(spaceY)+spaceY/2)*((i/Lx-spaceY/2)%(spaceY)+spaceY/2),1.0f)+powf32((Lz-(i/(Lx*Ly)))*(Lz-(i/(Lx*Ly))),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad) || 
     (powf32(((i%spaceX-spaceX))*((i%spaceX-spaceX)),1.0f)+powf32(((i/Lx-spaceY/2)%(spaceY)-spaceY/2)*((i/Lx-spaceY/2)%(spaceY)-spaceY/2),1.0f)+powf32((Lz-(i/(Lx*Ly)))*(Lz-(i/(Lx*Ly))),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad))
    //if(powf32(((i%spaceX)-spaceX/2)*((i%spaceX)-spaceX/2),1.0f)+powf32(((i/Lx)%(spaceY)-spaceY/2)*((i/Lx)%(spaceY)-spaceY/2),1.0f) < powf32((float)((spaceY)*(spaceY)),1.0f)*rad*rad)
    { 
      wall[i] = 'w';

      u[i] = 0.0f;
      u[i+L3] = 0.0f;      
      u[i+L3*2] = 0.0f;
      u[i+L3*3] = 0.0f;
      u[i+L3*4] = 0.0f;      
      u[i+L3*5] = 0.0f;

      th[i] = 0.0f;
      th[i+L3] = 0.0f;

      valOn--;

    }
    else
    {
      wall[i] = '0';

      th[i] = 1.0f;
      th[i+L3] = 1.0f;
    }
  }

  int xp,yp,zp,xm,ym,zm;

  for(int i = 0; i < Lx; i++)
  {
      for(int j = 0; j < Ly; j++)
      {
          for(int k = 0; k < Lz; k++)
          {   

              //Ignore if cell itself is a wall
              if(wall[i+j*Lx+k*Lx*Ly] != 'w')
              {
                  xp = (i+Lx+1)%Lx; xm = (i+Lx-1)%Lx;
                  yp = (j+Ly+1)%Ly; ym = (j+Ly-1)%Ly;
                  zp = (k+Lz+1)%Lz; zm = (k+Lz-1)%Lz;

                  //Check if any neighbour is a wall and mark it accordingly, there are 6 possible combinations that will be marked by the last 3 bits in the char string. (1<<0 is x direction, 1<<1 is y and 1<<2 is z).
                  if(wall[xm+j*Lx+k*Lx*Ly] == 'w')
                  {
                      wall[i+j*Lx+k*Lx*Ly] += (char)(1<<0);
                      valOn--;
                  }
                  else if(wall[xp+j*Lx+k*Lx*Ly] == 'w')
                  {
                      wall[i+j*Lx+k*Lx*Ly] += (char)(1<<0);
                      valOn--;
                  }

                  if(wall[i+ym*Lx+k*Lx*Ly] == 'w')
                  {
                      wall[i+j*Lx+k*Lx*Ly] += (char)(1<<1);
                  }
                  else if(wall[i+yp*Lx+k*Lx*Ly] == 'w')
                  {
                      wall[i+j*Lx+k*Lx*Ly] += (char)(1<<1);
                  }

                  if(wall[i+j*Lx+zm*Lx*Ly] == 'w')
                  {
                      wall[i+j*Lx+k*Lx*Ly] += (char)(1<<2);
                  }
                  else if(wall[i+j*Lx+zp*Lx*Ly] == 'w')
                  {
                      wall[i+j*Lx+k*Lx*Ly] += (char)(1<<2);
                  }
              }
          }
      }
  }

  //Export the wall array.
  exportBinary<char>(L3,"wall.raw",wall,1);

  std::cout << "Cells that are on: " << valOn << '\n';
  std::cout << "Porosity: " << ((T)valOn)/((T)L3) << '\n';

  //Initialize the renderer with the acceleration.
  Simulation::SimulationProperties *simProp = new Simulation::SimulationProperties();
  simProp->a = new float[3];
  simProp->a[0] = 0.00000000f;
  simProp->a[1] = 0.00000000f;
  simProp->a[2] = 0.00000000f;
  simProp->renderInt = 50.0f;
  simProp->visc = 0.5f*0.000375f;

  Render::Renderer* render = new Render::Renderer(new Render::WindowParams(1000,1000,"LBM-Window"),
                                                  new Render::GeometryParams(Lx,Ly,Lz,0.1f,0.1f,0.1f),
                                                  simProp);

  //Add a density renderobject as well.
  char* rhoRenderString[1] = {"Pressure"};
  Render::RenderObject *Pressure_renderObj = new Render::RenderObject("Shaders/shader_Pressure.vert",
                                                        "Shaders/shader_Pressure.frag",
                                                        1, L3, 2, "float",
                                                        rhoRenderString, false);

  render->addRenderObject(Pressure_renderObj);


  char* uRenderString[3] = {"u","v","w"};
  Render::RenderObject *U_renderObj = new Render::RenderObject("Shaders/shader.vert",
                                                        "Shaders/shader.frag",
                                                        3, L3, 6, "float",
                                                        uRenderString, true);

  render->addRenderObject(U_renderObj);


  char* tRenderString[1] = {"T"};
  Render::RenderObject *Temperature_renderObj = new Render::RenderObject("Shaders/shader_T.vert",
                                                          "Shaders/shader_T.frag",
                                                          1, L3, 2, "float",
                                                          tRenderString, true);

  render->addRenderObject(Temperature_renderObj);


  cudaMemcpy(wall_d, wall, L3*sizeof(char), cudaMemcpyHostToDevice);
  cudaDeviceSynchronize();


  dim3 block(Lx,1,1);
  dim3 grid(1,Ly,Lz);

  int nAvg = 0;
  clock_t time_old = clock();

  u_d = (T*)U_renderObj->getMappedPointer();
  p_d = (T*)Pressure_renderObj->getMappedPointer();
  th_d = (T*)Temperature_renderObj->getMappedPointer();

  cudaMemcpy(u_d, u, L3*sizeof(T)*6, cudaMemcpyHostToDevice);
  cudaMemcpy(p_d, p, L3*sizeof(T)*2, cudaMemcpyHostToDevice);

  cudaDeviceSynchronize();

  T oldVel = 0.0f;
  T vel = 0.0f;

  T pressure = 0.0f;
  T temperature = 0.0f;

  float tDiff = 0.003f;


  float force = 0.000001f;
  simProp->visc = 0.0012;

  float conv_monitor = 0.0f;
  float visc_monitor = 0.0f;
  float pg_monitor = 0.0f;

  float diss_monitor = 0.0f;
  float enstr_monitor = 0.0f;

  float temporal_monitor = 0.0f;


  std::vector<float> conv_arr, visc_arr, pg_arr, diss_arr, enstr_arr, vel_arr, p_arr, temp_arr, temporal_arr;

  for(int i = 0; i < 150000+1; i++)
  {
      
      queueStepCDS<T>(&block,&grid,u_d,u_d+3*L3, p_d, p_d+L3, th_d, th_d+L3, L3, Lx, Ly, Lz, wall_d, simProp->visc, tDiff, 0.0125f, 1.0f, force, 0.00001f);
      cudaDeviceSynchronize();
      queueSmooth<T>(&block, &grid, p_d+L3, p_d, wall_d, L3, Lx, Ly, Lz);
      cudaDeviceSynchronize();



      queueStepCDS<T>(&block,&grid,u_d+3*L3,u_d, p_d, p_d+L3, th_d+L3, th_d, L3, Lx, Ly, Lz, wall_d, simProp->visc, tDiff, 0.0125f, 1.0f, force, 0.00001f);
      cudaDeviceSynchronize();
      queueSmooth<T>(&block, &grid, p_d+L3, p_d, wall_d, L3, Lx, Ly, Lz);
      cudaDeviceSynchronize();

      
      //Update the velocity check.
      if(i%100 == 0)
      {

        U_renderObj->unMapPointer();
        Pressure_renderObj->unMapPointer();
        Temperature_renderObj->unMapPointer();

        render->view();
        render->pollEvents();
        
        //Get the velocity out.
        queueSumVelU<T>(arrLength, u_d, sumArray_d, L3); getError();
        cudaDeviceSynchronize();
        cudaMemcpy(sumArray, sumArray_d, arrLength*sizeof(T), cudaMemcpyDeviceToHost);
        std::cout << "It took: " << double(- time_old + clock()) / CLOCKS_PER_SEC << " --> ";
        time_old = clock();
        oldVel = vel;
        vel = 0.0f;

        for(int ii = 0; ii < arrLength; ii++)
        {
          vel += sumArray[ii];
        }

        //This is the darcy velocity.
        vel = vel/((T)valOn);

        //Check the average pressure...
        queueSumVelU<T>(arrLength, p_d, sumArray_d, L3); getError();
        cudaDeviceSynchronize();
        cudaMemcpy(sumArray, sumArray_d, arrLength*sizeof(T), cudaMemcpyDeviceToHost);

        pressure = 0.0f;

        for(int ii = 0; ii < arrLength; ii++)
        {
          pressure += sumArray[ii];
        }

        pressure = pressure/((T)valOn);


        //Check the average temperature...
        queueSumVelU<T>(arrLength, th_d, sumArray_d, L3); getError();
        cudaDeviceSynchronize();
        cudaMemcpy(sumArray, sumArray_d, arrLength*sizeof(T), cudaMemcpyDeviceToHost);

        temperature = 0.0f;

        for(int ii = 0; ii < arrLength; ii++)
        {
          temperature += sumArray[ii];
        }

        temperature = temperature/((T)valOn);


        std::cout << "Current avg velocity: " << vel << " --> Acc-x is: " << simProp->a[0] << " --> Visc is: " << simProp->visc << " Pressure avg is: " << pressure << " Temperature avg is: " << temperature << std::endl;

        //Add stuff to arrays.
        vel_arr.push_back(vel);
        p_arr.push_back(pressure);
        temp_arr.push_back(temperature);


        u_d = (T*)U_renderObj->getMappedPointer();
        p_d = (T*)Pressure_renderObj->getMappedPointer();
        th_d = (T*)Temperature_renderObj->getMappedPointer();

        //Get the data for the average values of the components.
        queueGetComponentsX<T>(&block,&grid,u_d,p_d,conv_d,visc_d,pg_d,wall_d,simProp->visc,Lx,Ly,Lz,L3);
        queueGetDissipationEnstrophy<T>(&block,&grid,u_d,p_d,diss_d,enstr_d,wall_d,simProp->visc,Lx,Ly,Lz,L3);
        queueGetTemporalFluctuation<T>(&block,&grid,u_d,u_d+3*L3,u_temporal_d,wall_d,simProp->visc,1.0f,Lx,Ly,Lz,L3);

        //Now read the components and extract the sum.
        conv_monitor = getSum(arrLength,conv_d,sumArray_d,sumArray,L3);
        visc_monitor = getSum(arrLength,visc_d,sumArray_d,sumArray,L3);
        pg_monitor = getSum(arrLength,pg_d,sumArray_d,sumArray,L3);

        diss_monitor = getSum(arrLength,diss_d,sumArray_d,sumArray,L3);
        enstr_monitor = getSum(arrLength,enstr_d,sumArray_d,sumArray,L3);

        temporal_monitor = getSum(arrLength,u_temporal_d,sumArray_d,sumArray,L3);


        //Just print another line for now.
        std::cout << "Conv: " << conv_monitor << "\tVisc is: " << visc_monitor << "\tpg is: "<< pg_monitor << "\t Sum is: " << conv_monitor - visc_monitor + pg_monitor << std::endl;

        std::cout <<  "Diss is: " << diss_monitor << "\tEnstr is: "<< enstr_monitor << "\tEnergy expr is: " << force*(vel*((float)valOn)/((float)L3))*((float)L3) << "\tTemporal fluctuation is: " << temporal_monitor << std::endl;


        //Push back the values.
        conv_arr.push_back(conv_monitor);
        visc_arr.push_back(visc_monitor);
        pg_arr.push_back(pg_monitor);
        diss_arr.push_back(diss_monitor);
        enstr_arr.push_back(enstr_monitor);
        temporal_arr.push_back(temporal_monitor);

        //Dump data every 2000 timesteps.
        if(i%2000 == 0)
        {
          //Dump the data.
          std::string lString = "data/vel_" + std::to_string(i/2000) + ".raw";
          dumpArray(u,u_d,L3,3,lString);

          //Also save the force components.
          queueGetComponents<T>(&block, &grid, u_d, p_d, force_all_d, force_all_d+3*L3, force_all_d+6*L3, wall_d, simProp->visc, Lx, Ly, Lz, L3);

          lString = "data/force_all_" + std::to_string(i / 2000) + ".raw";
          dumpArray(force_all_h, force_all_d, L3, 9, lString);

          lString = "data/p_" + std::to_string(i / 2000) + ".raw";
          dumpArray(p, p_d, L3, 1, lString);
          

        }

      }

  }

  //Dump the data from the monitors.
  exportBinary<T>(conv_arr.size(),"mon/conv.raw",&conv_arr[0],1);
  exportBinary<T>(visc_arr.size(),"mon/visc.raw",&visc_arr[0],1);
  exportBinary<T>(pg_arr.size(),"mon/pg.raw",&pg_arr[0],1);
  exportBinary<T>(diss_arr.size(),"mon/diss.raw",&diss_arr[0],1);
  exportBinary<T>(enstr_arr.size(),"mon/enstr.raw",&enstr_arr[0],1);

  exportBinary<T>(vel_arr.size(),"mon/vel.raw",&vel_arr[0],1);
  exportBinary<T>(temp_arr.size(),"mon/temp.raw",&temp_arr[0],1);
  exportBinary<T>(p_arr.size(),"mon/p.raw",&p_arr[0],1);

  exportBinary<T>(temporal_arr.size(),"mon/temporal.raw",&temporal_arr[0],1);

  cudaFree(wall_d);

  delete[] u;
  delete[] p;
  delete[] wall;

}
