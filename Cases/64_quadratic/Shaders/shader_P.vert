#version 460

in float posX;
in float posY;
in float posZ;
uniform mat4 MVP;
uniform float strength;

out vec4 colorParticle;

void main()
{
  gl_Position = MVP*(vec4( 0.1f*posX, 0.1f*posY, 0.1f*posZ, 1 ));
  colorParticle = vec4(1.0f,1.0f,1.0f,0.1f);
}
