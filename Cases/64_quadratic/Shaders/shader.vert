#version 460
in vec3 pos;

in float u;
in float v;
in float w;

uniform mat4 MVP;
uniform float strength;

out vec4 colorParticle;

void main()
{
  gl_Position = MVP*(vec4( pos.x, pos.y, pos.z, 1 ));
  //colorParticle = vec4(1.0f,1.0f,1.0f,1.0f);
  if(u == 0.0f)
  {
    colorParticle = 0.0f*vec4(0.0f,0.0f,1.0f,0.1f);
  }
  else
  {
    /*
    float intens = sqrt(u*u+v*v);
    float angle = (atan(u,v)+3.14)*sign(v);

    float C = intens;
    float X = C*(1.0-abs(mod(angle/(60.0*(2.0*3.14/360.0)),2)-1.0));
    float m = 1.0 - C;

    if(0 < angle && angle < 60*(2.0*3.14/360.0))
    {
      colorParticle = strength*vec4(C,X,0.0f,intens);
    }
    if(angle < 120*(2.0*3.14/360.0))
    {
      colorParticle = strength*vec4(X,C,0.0f,intens);
    }
    if(angle < 180*(2.0*3.14/360.0))
    {
      colorParticle = strength*vec4(0,C,X,intens);
    }
    if(angle < 240*(2.0*3.14/360.0))
    {
      colorParticle = strength*vec4(0,X,C,intens);
    }
    if(angle < 300*(2.0*3.14/360.0))
    {
      colorParticle = strength*vec4(X,0,C,intens);
    }
    else
    {
      colorParticle = strength*vec4(C,0,X,intens);
    }*/

    //colorParticle = colorParticle + strength*vec4(m,m,m,0.0f);

    colorParticle = strength*vec4(u*0.1f,v,w,1.0f);
  }

}
