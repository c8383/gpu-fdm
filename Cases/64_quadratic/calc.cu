/*

This source file is published as part of the ACM-FDM project.

Published under MIT license:

Copyright (c) 2022 https://gitlab.com/c8383/gpu-fdm

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


#include "calc.h"
#include "../../Kernels/FDM.h"
#include "../../Kernels/sumvel.h"
#include "../../Kernels/Util.h"


template<typename T>
void queueStepCDS(dim3* block, dim3* grid, T *ui, T *uo, T* pi, T* po, T* ti, T* to, int L3, int Lx, int Ly, int Lz, char* wall, T visc, T alpha, T urf, T dt, T ax, T at)
{
  stepCDS<T><<< *grid, *block>>>(ui, uo, pi, po, ti, to, L3, Lx, Ly, Lz, 0, 0, 0, wall, visc, alpha, urf, dt, ax, at);
}

template<typename T>
void queueSmooth(dim3* block, dim3* grid, T* pi, T* po, char* wall, int L3, int Lx, int Ly, int Lz)
{
  smooth<T><<<*grid, *block>>>(pi, po, wall, L3, Lx, Ly, Lz, 0, 0, 0);
}

template<typename T>
void queueSumVelU(int arrLength, T *u_d, T *sumArray_d, uint L3)
{
  sumVelU<T><<<arrLength, sumBlockSize>>>(u_d, sumArray_d, L3);
}

//Additional functions for channel stuff.
template<typename T>
void queueSumVelAll(int arrLength, T *u_d, T *sumArray_d, uint L3)
{
  sumVelAll<T><<<arrLength, sumBlockSize>>>(u_d, sumArray_d, L3);
}

template<typename T>
void queueSumVelAvg(int arrLength, T *u_d, T *sumArray_d, uint L3)
{
  sumVelAvg<T><<<arrLength, sumBlockSize>>>(u_d, sumArray_d, L3);
}

template<typename T>
void queueSumVelRS(uint arrLength, T *u_d, T *sumArray_d, uint L3)
{
  sumVelRS<T><<<arrLength, sumBlockSize>>>(u_d, sumArray_d, L3);
}

template<typename T>
void queueSumVelPV(uint arrLength, T *u_d, T *p_d, T *sumArray_d, uint L3)
{
  sumVelPV<T><<<arrLength, sumBlockSize>>>(u_d, p_d, sumArray_d, L3);
}

template<typename T>
void queueSumVelVUU(uint arrLength, T *u_d, T *sumArray_d, uint L3)
{
  sumVelVUU<T><<<arrLength, sumBlockSize>>>(u_d, sumArray_d, L3);
}

template<typename T>
void queueSumRhoAvg(int arrLength, T *rho_d, T *sumArray_d, uint L3)
{
  sumRhoAvg<T><<<arrLength, sumBlockSize>>>(rho_d, sumArray_d, L3);
}

//Components functions
template<typename T>
void queueGetComponentsX(dim3* block, dim3* grid, T *ui, T* pi, T* conv_o, T* visc_o, T* pg_o, char* wall, T visc, int Lx, int Ly, int Lz, int L3)
{
  getComponentsX<T><<<*grid, *block>>>(ui, pi, conv_o, visc_o, pg_o, wall, visc, Lx,Ly,Lz,L3);
}

template<typename T>
void queueGetComponents(dim3* block, dim3* grid, T *ui, T* pi, T* conv_o, T* visc_o, T* pg_o, char* wall, T visc, int Lx, int Ly, int Lz, int L3)
{
  getComponents<T><<<*grid, *block>>>(ui, pi, conv_o, visc_o, pg_o, wall, visc, Lx, Ly, Lz, L3);
}

template<typename T>
void queueGetDissipationEnstrophy(dim3* block, dim3* grid, T* ui, T* pi, T* diss_o, T* enstr_o, char *wall, T visc, int Lx, int Ly, int Lz, int L3)
{
  getDissipationEnstrophy<<<*grid, *block>>>(ui, pi, diss_o, enstr_o, wall, visc, Lx, Ly, Lz, L3);
}


//Function templates
//Doubles
template void queueStepCDS<double>(dim3* block, dim3* grid, double *ui, double *uo, double* pi, double* po, double* ti, double* to, int L3, int Lx, int Ly, int Lz, char* wall, double visc, double alpha, double urf, double dt, double ax, double at);
template void queueSmooth<double>(dim3* block, dim3* grid, double* pi, double* po, char* wall, int L3, int Lx, int Ly, int Lz);
template void queueSumVelAll<double>(int arrLength, double *u_d, double *sumArray_d, uint L3);
template void queueSumVelU<double>(int arrLength, double *u_d, double *sumArray_d, uint L3);
template void queueSumVelAvg<double>(int arrLength, double *u_d, double *sumArray_d, uint L3);
template void queueSumVelRS<double>(uint arrLength, double *u_d, double *sumArray_d, uint L3);
template void queueSumVelPV<double>(uint arrLength, double *u_d, double *p_d, double *sumArray_d, uint L3);
template void queueSumVelVUU<double>(uint arrLength, double *u_d, double *sumArray_d, uint L3);
template void queueSumRhoAvg<double>(int arrLength, double *rho_d, double *sumArray_d, uint L3);
template void queueGetComponentsX<double>(dim3* block, dim3* grid, double *ui, double* pi, double* conv_o, double* visc_o, double* pg_o, char* wall, double visc, int Lx, int Ly, int Lz, int L3);
template void queueGetComponents<double>(dim3* block, dim3* grid, double *ui, double* pi, double* conv_o, double* visc_o, double* pg_o, char* wall, double visc, int Lx, int Ly, int Lz, int L3);
template void queueGetDissipationEnstrophy<double>(dim3* block, dim3* grid, double* ui, double* pi, double* diss_o, double* enstr_o, char *wall, double visc, int Lx, int Ly, int Lz, int L3);

//Singles
template void queueSumVelU<float>(int arrLength, float *u_d, float *sumArray_d, uint L3);
template void queueStepCDS<float>(dim3* block, dim3* grid, float *ui, float *uo, float* pi, float* po, float* to, float* ti, int L3, int Lx, int Ly, int Lz, char* wall, float visc, float alpha, float urf, float dt, float ax, float at);
template void queueSmooth<float>(dim3* block, dim3* grid, float* pi, float* po, char* wall, int L3, int Lx, int Ly, int Lz);
template void queueSumVelAll<float>(int arrLength, float *u_d, float *sumArray_d, uint L3);
template void queueSumVelAvg<float>(int arrLength, float *u_d, float *sumArray_d, uint L3);
template void queueSumVelRS<float>(uint arrLength, float *u_d, float *sumArray_d, uint L3);
template void queueSumVelPV<float>(uint arrLength, float *u_d, float *p_d, float *sumArray_d, uint L3);
template void queueSumVelVUU<float>(uint arrLength, float *u_d, float *sumArray_d, uint L3);
template void queueSumRhoAvg<float>(int arrLength, float *rho_d, float *sumArray_d, uint L3);
template void queueGetComponentsX<float>(dim3* block, dim3* grid, float *ui, float* pi, float* conv_o, float* visc_o, float* pg_o, char* wall, float visc, int Lx, int Ly, int Lz, int L3);
template void queueGetComponents<float>(dim3* block, dim3* grid, float *ui, float* pi, float* conv_o, float* visc_o, float* pg_o, char* wall, float visc, int Lx, int Ly, int Lz, int L3);
template void queueGetDissipationEnstrophy<float>(dim3* block, dim3* grid, float* ui, float* pi, float* diss_o, float* enstr_o, char *wall, float visc, int Lx, int Ly, int Lz, int L3);